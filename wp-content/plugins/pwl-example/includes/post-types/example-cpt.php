<?php
		
	add_action( 'init', 'example_cpt' );
	
	/**
	 * Register the Shows post type.
	**/
	function example_cpt() {
		$labels = array(
			'name'               => _x( 'Examples', 'post type general name', 'example_cpt' ),
			'singular_name'      => _x( 'Example', 'post type singular name', 'example_cpt' ),
			'menu_name'          => _x( 'Examples', 'admin menu', 'example_cpt' ),
			'name_admin_bar'     => _x( 'Examples', 'add new on admin bar', 'example_cpt' ),
			'add_new'            => _x( 'Add New', 'Show', 'example_cpt' ),
			'add_new_item'       => __( 'Add New Example', 'example_cpt' ),
			'new_item'           => __( 'New Example', 'example_cpt' ),
			'edit_item'          => __( 'Edit Example', 'example_cpt' ),
			'view_item'          => __( 'View Example', 'example_cpt' ),
			'all_items'          => __( 'All Examples', 'example_cpt' ),
			'search_items'       => __( 'Search Examples', 'example_cpt' ),
			'parent_item_colon'  => __( 'Parent Example:', 'example_cpt' ),
			'not_found'          => __( 'No Example found.', 'example_cpt' ),
			'not_found_in_trash' => __( 'No Example found in Trash.', 'example_cpt' )
		);
	
		$args = array(
			'labels'             => $labels,
	        'description'        => __( 'Description.', 'example_cpt' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => true,
			'menu_position'      => null,
			'menu_icon'       	 => 'dashicons-tickets-alt',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
		);
	
		register_post_type( 'examples', $args );
	}

