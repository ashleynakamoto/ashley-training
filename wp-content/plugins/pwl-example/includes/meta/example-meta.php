<?php

add_action( 'cmb2_init', 'example_meta' );
	
function example_meta() {

	// Show Details
	$cmb_example = new_cmb2_box( array(
		'id'           => 'show_details',
		'title'        => 'Show Details',
		'object_types' => array( 'shows', ),
		'context'      => 'before_permalink',
	) );

	$cmb_example->add_field( array(
		'name' => 'Sizzle Link',
		'id'   => 'shows_sizzle',
		'type' => 'text',
		'desc' => 'URL to the show sizzle on Vimeo / YouTube.',
	) );

	$cmb_example->add_field( array(
		'name' => 'Format',
		'id'   => 'shows_format',
		'type' => 'text',
		'desc' => 'Example: Primetime Entertainment Format for ITV (60)',
	) );

	$cmb_example->add_field( array(
		'name' => 'Territories',
		'id'   => 'shows_territories',
		'type' => 'text',
		'desc' => 'Example: Produced in 12 territories',
	) );

	$cmb_example->add_field( array(
		'name' => 'Ratings',
		'id'   => 'shows_ratings',
		'type' => 'text',
		'desc' => 'Example: Ratings info',
	) );

	$cmb_example->add_field( array(
		'name' => 'Similar Show Link',
		'id'   => 'shows_showlink',
		'type' => 'text',
		'desc' => 'URL to a similar show / spin off series.',
	) );

	$cmb_example->add_field( array(
		'name'    => 'Open Applications',
		'id'      => 'shows_applications',
		'type'    => 'radio_inline',
		'options' => array(
			'no' => __( 'No', 'cmb2' ),
			'yes'   => __( 'Yes', 'cmb2' ),
		),
		'default' => 'no',
	) );

	$cmb_example->add_field( array(
		'name' => 'Application Page Link',
		'id'   => 'shows_applicationlink',
		'type' => 'text_url',
		'desc' => 'URL to the shows application form.',
	) );
	
}