<?php
	
/**
* Plugin Name: PWL - Example CPT/Meta Plugin
**/

/**
* stop direct access to the plugin file
**/
if ( ! defined( 'WPINC' ) ) { die; }

/**
* define plugin directory
**/
define( 'PLUGIN_DIR', plugin_dir_path( __FILE__ ) ); 


if ( file_exists( PLUGIN_DIR . '/vendor/cmb2/init.php' ) ) {
	require_once PLUGIN_DIR . '/vendor/cmb2/init.php';
} elseif ( file_exists( PLUGIN_DIR . '/vendor/CMB2/init.php' ) ) {
	require_once PLUGIN_DIR . '/vendor/CMB2/init.php';
}

require_once PLUGIN_DIR . '/includes/post-types/shows-cpt.php';
require_once PLUGIN_DIR . '/includes/post-types/team-cpt.php';

require_once PLUGIN_DIR . '/includes/meta/shows-meta.php';
require_once PLUGIN_DIR . '/includes/meta/team-meta.php';
