<?php

// =============================================================================
// TEMPLATE NAME: Blank - No Container | Header, Footer
// -----------------------------------------------------------------------------
// A blank page for creating unique layouts.
//
// Content is output based on which Stack has been selected in the Customizer.
// To view and/or edit the markup of your Stack's index, first go to "views"
// inside the "framework" subdirectory. Once inside, find your Stack's folder
// and look for a file called "template-blank-4.php," where you'll be able to
// find the appropriate output.
// =============================================================================

get_header(); ?>

<div class="x-main full" role="main">

	<?php while ( have_posts() ) : the_post(); ?>

		<?php $featured_img_url = get_the_post_thumbnail_url( get_the_ID(), 'full' ); ?>

		<div class="x-section section--hero" style="margin: 0 0 1em; padding: 0;">
			<div class="x-bg" aria-hidden="true" data-x-element="bg">
				<div class="x-bg-layer-lower-image" style="background-image: url(<?php echo esc_url( $featured_img_url ); ?>); background-repeat: no-repeat; background-position: center; background-size: cover;"></div>
			</div>
			<div class="x-container max width">
				<div class="x-column x-sm x-1-1">&nbsp;</div>
			</div>
		</div>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php the_content();?>
		</article>

	<?php endwhile; ?>

</div>

<?php get_footer(); ?>