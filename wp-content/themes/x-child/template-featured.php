<?php

// =============================================================================
// TEMPLATE NAME: 01 - Featured Image Template
// =============================================================================

?>

<?php get_header(); ?>

  <div class="x-main full" role="main">

  	<?php while ( have_posts() ) : the_post();

    $pageID = get_option('page_for_posts');
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $pageID ), 'single-post-thumbnail' );
    $image = $image[0];

    if ( $image ) { ?>
      <div class="page-featured x-container max width" style="background:url(<?php echo $image; ?>) no-repeat;">
        <h1 class="h-custom-headline"><span><?php the_title(); ?></span></h2>
      </div>
    <?php } ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php x_get_view( 'global', '_content', 'the-content' ); ?>
      </article>

    <?php endwhile; ?>

  </div>

<?php get_footer(); ?>