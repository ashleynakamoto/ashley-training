<div class="x-column x-sm x-1-1 team">
<?php if ( $query->have_posts() ) {

	while ($query->have_posts()) {

		$query->the_post();

			$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
			$thumb_url = $thumb_url_array[0];
			$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$job_title = get_field( "job_title" );
			?>

			<div class="x-column x-sm x-1-3">
		
					<div class="post-top" style="background: url('<?php echo $backgroundImg[0]; ?>') no-repeat;"></div>
					<div class="post-bottom">
						<h4 class="merchant-title"><?php the_title();?></h4>
						<strong><?php echo $job_title; ?></strong>
						<a href="<?php the_permalink(); ?>" class="read-more popmake-post-<?php the_ID();?>">  
							<?php echo do_shortcode("[popup id='post-". get_the_ID() ."' size='small']". '<div class="x-column x-sm x-1-1"><img src="' . $image_url . '" /></div><div class="x-column x-sm x-1-1"><h4>' . get_the_title() . '</h4>' . get_the_content() . "</div>[/popup]");?>
						Read More</a>
						               
					</div>

			</div>

<?php } ?>
</div>

<div class="pagination">
	<?php
	if (function_exists('wp_pagenavi')) {
		wp_pagenavi( array( 'query' => $query ) );
	}
	?>
</div>

<?php
} else {
	echo "No results found";
}
?>