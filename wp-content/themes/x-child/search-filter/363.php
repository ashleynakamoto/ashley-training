<div class="x-column x-sm x-1-1 news">
<?php if ( $query->have_posts() ) {

	while ($query->have_posts()) {

		$query->the_post();

			$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
			$thumb_url = $thumb_url_array[0];
			$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			?>

			<div class="x-column x-sm x-1-3">

				<a href="<?php the_permalink(); ?>" class="merchant-inner">
					<div class="post-top" style="background: url('<?php echo $backgroundImg[0]; ?>') no-repeat;"></div>
					<div class="post-bottom">
						<h4 class="merchant-title"><?php the_title();?></h4>
						<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
					</div>
				</a>

			</div>

<?php } ?>
</div>

<div class="pagination">
	<?php
	if (function_exists('wp_pagenavi')) {
		wp_pagenavi( array( 'query' => $query ) );
	}
	?>
</div>

<?php
} else {
	echo "No results found";
}
?>