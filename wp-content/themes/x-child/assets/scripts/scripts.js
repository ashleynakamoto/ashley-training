jQuery(document).ready(function($){

	/**
	 * Mobile Menu Toggle
	 *
	 * Adds class to body tag to indicate mobile menu has been toggled
	 */

	$('.x-btn-navbar').on('click', function () {
		$('body').toggleClass('mobile-menu-open');
	});

});
