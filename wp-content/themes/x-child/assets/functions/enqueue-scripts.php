<?php 

/**
 * Security Check - bail on direct file access
 **/

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' ); 


/**
 * Enqueue child theme stylesheet
 **/
add_action('wp_enqueue_scripts', 'enqueue_theme_scripts_styles', 10);

function enqueue_theme_scripts_styles(){
    wp_enqueue_style( 'child-theme', get_stylesheet_directory_uri() . '/dist/css/style.min.css', array(), '', 'all' );
    wp_enqueue_script( 'child-theme-js', get_stylesheet_directory_uri() . '/dist/scripts/scripts.min.js', array( 'jquery' ), '1.0', true );
}
