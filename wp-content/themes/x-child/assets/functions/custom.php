<?php 

/**
 * Allow SVG Images in Media Library
 **/

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/**
 * Unregistering Portfolio CPT
 **/

add_action( 'after_setup_theme','remove_portfolio_cpt', 100 );

function remove_portfolio_cpt() {   
  remove_action( 'init', 'x_portfolio_init');    
}


/**
 * WP_Head Scripts
 **/

function head_scripts() {
    echo "";
}
add_filter('wp_head', 'head_scripts', 999);


/**
 * WP_Footer Scripts
 **/

function footer_scripts() {
    echo "";
}
add_filter('wp_footer', 'footer_scripts', 999);

/**
 * Shortcode to output an accordion navigation menu
 **/

function print_menu_shortcode($atts, $content = null) {
    extract( shortcode_atts(array( 'menu' => null, 'name' => null ), $atts) );

    $output .= '[accordion_item title="' . $name . '" parent_id="footer_mobile_menu"]';
        $output .= wp_nav_menu( array( 'menu' => $menu, 'echo' => false ) );
    $output .= '[/accordion_item]';
    return do_shortcode('[accordion id="footer_mobile_menu"]' . $output . '[/accordion]');
}
add_shortcode('menu', 'print_menu_shortcode');