<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );



// Additional Functions
// =============================================================================

// Scripts and Stylesheets
require_once( get_stylesheet_directory().'/assets/functions/enqueue-scripts.php' );
require_once( get_stylesheet_directory().'/assets/functions/custom.php' );
require_once( get_stylesheet_directory().'/assets/functions/widgets.php' );

// Gravity Forms labels
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
