<?php

// =============================================================================
// TEMPLATE NAME: Blank - Container | Header, Footer
// -----------------------------------------------------------------------------
// A blank page for creating unique layouts.
//
// Content is output based on which Stack has been selected in the Customizer.
// To view and/or edit the markup of your Stack's index, first go to "views"
// inside the "framework" subdirectory. Once inside, find your Stack's folder
// and look for a file called "template-blank-4.php," where you'll be able to
// find the appropriate output.
// =============================================================================

get_header(); ?>

<div class="content">
	<div id="cs-content" class="cs-content">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php $featured_img_url = get_the_post_thumbnail_url( get_the_ID(), 'full' ); ?>

			<div class="x-section section--hero" style="margin: 0 0 1em; padding: 0;">
				<div class="x-bg" aria-hidden="true" data-x-element="bg">
					<div class="x-bg-layer-lower-image" style="background-image: url(<?php echo esc_url( $featured_img_url ); ?>); background-repeat: no-repeat; background-position: center; background-size: cover;"></div>
				</div>
				<div class="x-container max width">
					<div class="x-column x-sm x-1-1">&nbsp;</div>
				</div>
			</div>

			<div class="x-container max width offset offset-column-sidebar" style="position: relative;">

				<div class="x-column x-sm x-1-1">
						<div class="x-column x-sm x-1-1">
							<h1><?php the_title();?></h1>
						</div>
					<div class="x-column x-sm x-1-5">
							
						<div class="section--social-share">
							<ul>
								<li class="share__facebook">
									<a href="https://www.facebook.com/sharer.php?u=<?php echo esc_url( get_the_permalink() ); ?>" target="_blank">
										<i class="x-icon x-icon-facebook-f" data-x-icon-b="" aria-hidden="true"></i>
										<span class="screen-reader-text">Share via Facebook</span>
									</a>
								</li>
								<li class="share__twitter">
									<a href="https://twitter.com/share?text=<?php echo esc_html( get_the_title() ); ?>&url=<?php echo esc_url( get_the_permalink() ); ?>" target="_blank">
										<i class="x-icon x-icon-twitter" data-x-icon-b="" aria-hidden="true"></i>
										<span class="screen-reader-text">Share via Twitter</span>
									</a>
								</li>
							</ul>
						</div><!-- .section--social-share -->

					</div>

					<div class="x-column x-sm x-3-5 entry-content">
						<?php the_content();?>
					</div>

					<div class="x-column x-sm x-1-5">
						<?php dynamic_sidebar('sidebar-main'); ?>
					</div>
				</div>


			</div>

		<?php endwhile; ?>

	</div>
</div>

<?php get_footer(); ?>