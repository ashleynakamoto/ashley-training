// Load dependencies
var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    sassGlob     = require('gulp-sass-glob'),
    less         = require('gulp-less'),
    lessGlob     = require('gulp-less-glob'),
    autoprefixer = require('gulp-autoprefixer'),
    gutil        = require('gulp-util'),
    cleanCSS     = require('gulp-clean-css'),
    sourcemaps   = require('gulp-sourcemaps'),
    notify       = require('gulp-notify'),
    concat       = require('gulp-concat'),
    rename       = require('gulp-rename'),
    jshint       = require('gulp-jshint'),
    stylish      = require('jshint-stylish'),
    babel        = require('gulp-babel'),
    uglify       = require('gulp-uglify'),
    imagemin     = require('gulp-imagemin'),
    cache        = require('gulp-cache'),
    phplint      = require('gulp-phplint'),
    browserSync  = require('browser-sync').create();

/* ---------------------------------------------------------------------------
CONFIGURATION
Edit the values in this object to match your project structure
------------------------------------------------------------------------------ */
var config = {
	scss: {
		active: true, // By default our gulpfile processes SCSS
		sourceFile: 'assets/scss/style.scss', // Source SCSS FILE containing your @imports
		sourceDirectory: 'assets/scss', // Source DIRECTORY - used to watch for file changes
		targetDirectory: 'dist/css', // Destination DIRECTORY
	},
	less: {
		active: false, // If you want to process LESS, change this to true
		sourceFile: 'assets/scss/style.less', // Source LESS FILE containing your @imports
		sourceDirectory: 'assets/scss', // Source DIRECTORY - used to watch for file changes
		targetDirectory: 'dist/css', // Destination DIRECTORY
	},
	scripts: {
		sourceFile: 'assets/scripts/**/*.js', // Source FILE or FILES(glob) for JS
		targetDirectory: 'dist/scripts', // Destination DIRECTORY
	},
	images: {
		sourceFile: 'assets/images/*.*', // Source FILE or FILES(glob) for theme images
		targetDirectory: 'dist/images', // Destination DIRECTORY
	},
	php: {
		sourceFiles: ['./**/*.php', '!./node_modules/**/*.php'] // Array of PHP FILES to lint
	},
	browsersync: {
		active: true, // Set to TRUE to enable Browsersync
		proxy: 'https://EXAMPLE-SITE.dev', // Set this to the local URL for this project
		reloadDirectory: 'dist/**' // Reload the browser when changes occur in this DIRECTORY
	}
}
/* ---------------------------------------------------------------------------
!!! YOU SHOULDN'T NEED TO EDIT BELOW THIS LINE !!!
... unless you want to change the actual tasks or add a new task for your project,
... if you need to do this, please submit your changes as a pull request to:
... https://bitbucket.org/pragmaticweb/tech-gulpjs
------------------------------------------------------------------------------ */


// Process SCSS
// ------------
gulp.task( 'scss', function() {
	return gulp.src( config.scss.sourceFile )
		// Allow for globbed @imports in SCSS and sourcemaps
		.pipe( sassGlob() )
		.pipe( sourcemaps.init() )
		// Print SASS errors to console
		.pipe( sass().on( 'error', sass.logError ) )
		// Write sourcemaps
		.pipe( sourcemaps.write() )
		// Autoprefix
		.pipe(
			autoprefixer(
				'last 2 version',
				'safari 5',
				'ie 8',
				'ie 9',
				'opera 12.1',
				'ios 6',
				'android 4'
			)
		)
		// Minify
		.pipe( cleanCSS( { compatibility: '*' } ) )
		// Rename file to .min.css
		.pipe( rename( { suffix: '.min' } ) )
		// Put resulting file in target directory
		.pipe( gulp.dest( config.scss.targetDirectory ) )
		// Browsersync
		.pipe( browserSync.stream() )
		.pipe( notify( { message: 'Sass completed' } ) );
}); // Process SCSS



// Process LESS
// ------------
gulp.task( 'less', function() {
	return gulp.src( config.less.sourceFile )
		// Allow for globbed @imports in LESS and sourcemaps
		.pipe( lessGlob() )
		.pipe( sourcemaps.init() )
		// Print LESS errors to console
		.pipe( less().on( 'error', gutil.log ) )
		// Write sourcemaps
		.pipe( sourcemaps.write() )
		// Autoprefix
		.pipe(
			autoprefixer(
				'last 2 version',
				'safari 5',
				'ie 8',
				'ie 9',
				'opera 12.1',
				'ios 6',
				'android 4'
			)
		)
		// Minify
		.pipe( cleanCSS( { compatibility: '*' } ) )
		// Rename file to .min.css
		.pipe( rename( { suffix: '.min' } ) )
		// Put resulting file in target directory
		.pipe( gulp.dest( config.less.targetDirectory ) )
		// Browsersync
		.pipe( browserSync.stream() )
		.pipe( notify( { message: 'Less completed' } ) );
}); // Process LESS



// Process JS
// ----------
gulp.task( 'scripts', function() {
	return gulp.src( config.scripts.sourceFile )
		// Print JS errors to console
		.on( 'error', gutil.log )
		// Sourcemaps
		.pipe( sourcemaps.init() )
		// Print JS hints to console
		.pipe( jshint() )
		.pipe( jshint.reporter( stylish ) )
		// Transpile any ES2015 syntax into browser-ready JS
		.pipe( babel( { presets: ['es2015-no-strict'] } ) )
		// Minify
		.pipe( uglify() )
		// Concatenate, rename file, write sourcemaps
		.pipe( concat( 'scripts.js' ) )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( sourcemaps.write( '.' ) )
		// Put resulting file in destination
		.pipe( gulp.dest( config.scripts.targetDirectory ) )
		.pipe( notify( { message: 'Scripts completed' } ) );
}); // Process JS



// Process Images
// --------------
gulp.task( 'images', function() {
	return gulp.src( config.images.sourceFile )
		.pipe(
			cache(
				imagemin({
					optimizationLevel: 3,
					progressive: true,
					interlaced: true
				})
			)
		)
		.pipe( gulp.dest( config.images.targetDirectory ) )
		.pipe( notify( { message: 'Image compression completed' } ) );
}); // Process Images



// Lint for PHP errors
// -------------------
gulp.task( 'phplint', function() {
	return gulp.src( config.php.sourceFiles )
		.pipe( phplint( '', { notify: true } ) )
		.pipe( phplint.reporter( 'fail' ) );
}); // Lint for PHP errors



// Browsersync
// -----------
gulp.task( 'browser-sync', function() {
	browserSync.init({
		proxy: config.browsersync.proxy
	});
}); // Browsersync



// Default task order
// ------------------
gulp.task( 'default', function() {
	if ( config.scss.active ) {
		if ( config.browsersync.active ) {
			gulp.start( 'scss', 'scripts', 'images', 'phplint', 'browser-sync', 'watch' );
		} else {
			gulp.start( 'scss', 'scripts', 'images', 'phplint', 'watch' );
		}
	} else if ( config.less.active ) {
		if ( config.browsersync.active ) {
			gulp.start( 'less', 'scripts', 'images', 'phplint', 'browser-sync', 'watch' );
		} else {
			gulp.start( 'less', 'scripts', 'images', 'phplint', 'watch' );
		}
	}
}); // Default task order



// Watch files
// -----------
gulp.task( 'watch', function() {
	// Watch SCSS files
	gulp.watch( config.scss.sourceDirectory + '/**/*.scss', ['scss'] );
	// Watch LESS files
	gulp.watch( config.less.sourceDirectory + '/**/*.less', ['less'] );
	// Watch JS files
	gulp.watch( config.scripts.sourceFile, ['scripts'] );
	// Watch Images
	gulp.watch( config.images.sourceFile, ['images'] );
	// Watch Theme PHP
	gulp.watch( './**/*.php', ['phplint'] );

	if ( config.browsersync.active ) {
		// Watch any files in dist/, reload on change
		gulp.watch( config.browsersync.reloadDirectory ).on( 'change', browserSync.reload );
	}
}); // Watch files
