<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

<div id="tribe-events-content" class="tribe-events-single x-container">

	<div class="e436-106 x-column x-sm x-2-3">

		<!-- Notices -->
		<?php tribe_the_notices() ?>

		<?php the_title( '<h1 class="tribe-events-single-event-title">', '</h1>' ); ?>

		<div class="tribe-events-schedule tribe-clearfix">
			<?php echo tribe_events_event_schedule_details( $event_id, '<h2>', '</h2>' ); ?>
			<?php if ( tribe_get_cost() ) : ?>
				<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>
			<?php endif; ?>
		</div>

		<?php while ( have_posts() ) :  the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<!-- Event content -->
				<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
				<div class="tribe-events-single-event-description tribe-events-content">
					<?php the_content(); ?>
				</div>
				<!-- .tribe-events-single-event-description -->
				<?php //do_action( 'tribe_events_single_event_after_the_content' ) ?>

				<!-- Before meta -->
				<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
				<!-- Related events -->
				<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>

				<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
			</div>
	</div>

	<div class="e436-114 x-column x-sm x-1-3">
		<!-- Event meta -->
		<?php tribe_get_template_part( 'modules/meta' ); ?>
	</div>

		<?php endwhile; ?>

</div><!-- #tribe-events-content -->