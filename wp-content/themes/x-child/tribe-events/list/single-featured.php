<?php
/**
 * List View Single Featured Event
 * This file contains one featured event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-featured.php
 *
 * @version 4.6.19
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

// Thumbnail
$thumb_id = get_post_meta(get_the_ID(), 'post_thumbnail_image', true);
if ( empty( $thumb_id ) ) {
  $thumb_id = get_post_thumbnail_id();
}
$thumb_image = wp_get_attachment_image( $thumb_id, 'tile-thumbnail', "", array( "class" => "post-featured-image" ) );
?>

<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
	<?php echo $thumb_image; ?>
</a>

<div class="post-content">

	<!-- Event Title -->
	<?php do_action( 'tribe_events_before_the_event_title' ) ?>
	<h3 class="tribe-events-list-event-title">
		<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
			<?php the_title() ?>
		</a>
	</h3>
	<?php do_action( 'tribe_events_after_the_event_title' ) ?>

	<!-- Event Content -->
	<?php do_action( 'tribe_events_before_the_content' ) ?>
	<div class="tribe-events-list-event-description tribe-events-content">
		<?php echo tribe_events_get_the_excerpt( null, wp_kses_allowed_html( 'post' ) ); ?>
	</div><!-- .tribe-events-list-event-description -->
	<?php
	do_action( 'tribe_events_after_the_content' );
	?>

	<!-- Event Meta -->
	<?php do_action( 'tribe_events_before_the_meta' ) ?>
	<div class="tribe-events-event-meta">
		<div class="author <?php echo esc_attr( $has_venue_address ); ?>">

			<!-- Schedule & Recurrence Details -->
			<div class="tribe-event-schedule-details">
				<label>Date:</label> <?php echo tribe_events_event_schedule_details() ?>
			</div>

			<?php if ( $venue_details ) : ?>
				<!-- Venue Display Info -->
				<div class="tribe-events-venue-details">
					<label>Location:</label> <?php echo implode( ', ', $venue_details ); ?>
					<?php
					if ( tribe_show_google_map_link() ) {
						echo tribe_get_map_link_html();
					}
					?>
				</div> <!-- .tribe-events-venue-details -->
			<?php endif; ?>

		</div>
	</div><!-- .tribe-events-event-meta -->
	<?php do_action( 'tribe_events_after_the_meta' ) ?>

	<!-- Event Cost -->
	<?php if ( tribe_get_cost() ) : ?>
		<div class="tribe-events-event-cost featured-event">
			<span class="ticket-cost"><label>Cost:</label> <?php echo esc_html( tribe_get_cost( null, true ) ); ?></span>
			<?php
			/** This action is documented in the-events-calendar/src/views/list/single-event.php */
			do_action( 'tribe_events_inside_cost' )
			?>
		</div>
	<?php endif; ?>

</div>
